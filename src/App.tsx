import {useState} from 'react';
import {useQuery} from 'react-query';
import Drawer from '@material-ui/core/Drawer';
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Badge from '@material-ui/core/Badge';
import { Wrapper, StyledButton } from './App.styles';
import { makeStyles } from '@material-ui/core/styles';
import Elemento from './Elemento/Elemento';
import Carrito from './Carrito/Carrito';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';


export type TipoElementoCarrito ={
    id: number;
    miniatura: string;
    nombre: string;
    descripcion: string;
    precio: number,
    cantidad: number
}
const useStylesCardModal = makeStyles({
  root: {
    maxWidth: 450,
  },
  media: {
    height: 150,
    maxHeight: '250px',
    backgroundSize: 'contain',
  },
  button:{
    borderRadius: '0 0 20px 20px'
  }
});
const useStylesModal = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }
}));

const obtenerProductos = async (): Promise<TipoElementoCarrito[]> =>
  await( await fetch('http://localhost:3000/data.json')).json(); 
const AplicationUTP = () =>{
  const classesModal= useStylesModal();
  const classesCardModal= useStylesCardModal();
  const [carritoMostrar, setCarritoMostrar] = useState(false);
  const [confirmarEliminar, setConfirmarEliminar] = useState(false);
  const [idEliminar, setidEliminar] = useState(null);
  const [elementosCarrito, setElementosCarrito] = useState([] as TipoElementoCarrito[]);
  const {data,isLoading, error} = useQuery<TipoElementoCarrito[]>(
    'productos',
    obtenerProductos
    );
    console.log(data);
  
    const obtenerTotalElementos = (elementos: TipoElementoCarrito[]) =>
    elementos.reduce((ack: number, elemento) => ack + elemento.cantidad, 0);

    const anadirCarritoFnApp = (elementoSeleccionado: TipoElementoCarrito) => {
      setElementosCarrito(prev => {
        const elementoExistente = prev.find(elemento => elemento.id === elementoSeleccionado.id);
  
        if (elementoExistente) {
          setConfirmarEliminar(true);
          setidEliminar(elementoSeleccionado.id);
           

          return prev.map(elemento =>
            elemento.id === elementoSeleccionado.id
              ? { ...elemento, cantidad: elemento.cantidad }
              : elemento
          );

        }
        return [...prev, { ...elementoSeleccionado, cantidad: 1 }];
      });
    };

    const removerModal = ()=>{
       if(idEliminar != null){
         removerCarrito(idEliminar);
       }
       setConfirmarEliminar(false);
    }


    const removerCarrito = (id: number) => {
      setElementosCarrito(prev =>
        prev.reduce((ack, elemento) => {
          if (elemento.id === id) {
            if (elemento.cantidad === 1) return ack;
            return [...ack, { ...elemento, cantidad: elemento.cantidad - 1 }];
          } else {
            return [...ack, elemento];
          }
        }, [] as TipoElementoCarrito[])
      );
    };
    
 
    if(isLoading) return <LinearProgress/>
    if(error) return <div>!Hubo un problema!</div>
  return (
    <div>
        <AppBar position="static">
        <Toolbar variant="dense">
          <IconButton edge="start"  color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit">
            UTP Productos
          </Typography>
        </Toolbar>
      </AppBar>
        <Wrapper>

        <Drawer anchor='right' open={carritoMostrar} onClose={() => setCarritoMostrar(false)}>
          <Carrito
            elementosCarrito={elementosCarrito}
            anadirCarrito={anadirCarritoFnApp}
            removerCarrito={removerCarrito}
          />
        </Drawer>
        <StyledButton onClick={() => setCarritoMostrar(true)}>
          <Badge badgeContent={obtenerTotalElementos(elementosCarrito)} style={{ color: 'white' }}>
            <AddShoppingCartIcon />
          </Badge>
        </StyledButton>
        <Grid container spacing={3}>
          {data?.map(elemento => (
            <Grid item key={elemento.id} xs={12} sm={3}>
              <Elemento elemento={elemento} anadirCarritoFn={anadirCarritoFnApp}  />
            </Grid>
          ))}
        </Grid>
        <Modal className={classesModal.modal}
            open={confirmarEliminar}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            >
                <Card className={classesCardModal.root}>
                    <CardActionArea>
                    <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                      Lo sentimos solo puede añadir un solo elemento por producto. ¿Desea eliminarlo?
                    </Typography>
                    </CardContent>
                    </CardActionArea>
                    <CardActions>
                    <Button variant="outlined" color="primary" startIcon={<DeleteIcon />} onClick={removerModal}>
                      Eliminar
                    </Button>
                    </CardActions>
                </Card>
            </Modal>
      </Wrapper>
  </div>
  );
}

export default AplicationUTP;
