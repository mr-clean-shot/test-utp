import Button from '@material-ui/core/Button';
import { TipoElementoCarrito } from '../App';
import { Wrapper } from './ElementoCarrito.styles';

type Props = {
  elemento: TipoElementoCarrito;
  anadirCarrito: (elementoSeleccionado: TipoElementoCarrito) => void;
  removerCarrito: (id: number) => void;
};

const ElementoCarrito: React.FC<Props> = ({ elemento, anadirCarrito, removerCarrito }) => (
  <Wrapper>
    <div>
      <h3>{elemento.nombre}</h3>
      <div className='information'>
        <p>Precio: S/. {elemento.precio}</p>
        <p>Total: S/. {(elemento.cantidad * elemento.precio).toFixed(2)}</p>
      </div>
      <div className='buttons'>
        <Button
          size='small'
          disableElevation
          variant="contained" color="secondary"
          onClick={() => removerCarrito(elemento.id)}
        >-</Button>
        <p>{elemento.cantidad}</p>
        <Button 
        size='small'
        disableElevation
        variant="contained" color="secondary"
        onClick={() => anadirCarrito(elemento)}
      >+</Button>    
      </div>
    </div>
    <img src={elemento.miniatura} alt={elemento.nombre} />
  </Wrapper>
);

export default ElementoCarrito;