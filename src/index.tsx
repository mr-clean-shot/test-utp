import ReactDOM from 'react-dom';
import React from 'react';
import AplicationUTP from './App';
import {QueryClient, QueryClientProvider} from 'react-query';
import { GlobalStyle } from './App.styles';
const utp = new QueryClient();

ReactDOM.render(
<QueryClientProvider client={utp}>
  <GlobalStyle />
  <AplicationUTP />  
</QueryClientProvider>
,document.getElementById('root'));


