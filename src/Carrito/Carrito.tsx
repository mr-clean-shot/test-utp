import ElementoCarrito from '../ElementoCarrito/ElementoCarrito';
import { Wrapper } from './Carrito.styles';
import { TipoElementoCarrito } from '../App';

type Props = {
  elementosCarrito:  TipoElementoCarrito[];
  anadirCarrito: (elementoSeleccionado:  TipoElementoCarrito) => void;
  removerCarrito: (id: number) => void;
};

const Carrito: React.FC<Props> = ({ elementosCarrito, anadirCarrito, removerCarrito }) => {
  const calculateTotal = (elementos: TipoElementoCarrito[]) =>
    elementos.reduce((ack: number, elemento) => ack + elemento.cantidad * elemento.precio, 0);

  return (
    <Wrapper>
      <h2>Carrito de Compras</h2>
      {elementosCarrito.length === 0 ? <p>Tu carrito esta vacio. Añade algun producto y lo veras</p> : null}
      {elementosCarrito.map(elemento => (
        <ElementoCarrito
          key={elemento.id}
          elemento={elemento}
          anadirCarrito={anadirCarrito}
          removerCarrito={removerCarrito}
        />
      ))}
      <h2>Total: S/. {calculateTotal(elementosCarrito).toFixed(2)}</h2>
    </Wrapper>
  );
};

export default Carrito;