import { TipoElementoCarrito } from '../App';
import { Wrapper } from '../App.styles';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import {useState} from 'react';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import VisibilityIcon from '@material-ui/icons/Visibility';
const useStylesCard = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 200,
    maxHeight: '250px',
    backgroundSize: 'contain',
  },
  content:{
    minHeight: '150px',
  }

});

const useStylesCardModal = makeStyles({
    root: {
      maxWidth: 450,
    },
    media: {
      height: 150,
      maxHeight: '250px',
      backgroundSize: 'contain',
    },
    button:{
      borderRadius: '0 0 20px 20px'
    }
});

type Props = {
    elemento: TipoElementoCarrito,
    anadirCarritoFn: (elementoSeleccionado: TipoElementoCarrito) => void
}

const useStylesModal = makeStyles((theme) => ({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    }
  }));

  const themeModal = createMuiTheme();

  themeModal.typography.h3 = {
    fontSize: '1.5rem',
    '@media (min-width:600px)': {
      fontSize: '1rem',
    },
    [themeModal.breakpoints.up('md')]: {
      fontSize: '1.4rem',
    },
  };

  themeModal.typography.body2 = {
    fontSize: '0.7rem',
    margin: '20px 0px',
    '@media (min-width:600px)': {
      fontSize: '0.7rem',
    },

    [themeModal.breakpoints.up('md')]: {
      fontSize: '0.7rem',
    },
  };

  const themeCard = createMuiTheme();

  themeCard.typography.h3 = {
    fontSize: '1.5rem',
    '@media (min-width:600px)': {
      fontSize: '1rem',
    },
    [themeCard.breakpoints.up('md')]: {
      fontSize: '1.4rem',
    },
  };

  themeCard.typography.body2 = {
    fontSize: '0.7rem',
    margin: '20px 0px',
    '@media (min-width:600px)': {
      fontSize: '0.7rem',
    },

    [themeCard.breakpoints.up('md')]: {
      fontSize: '0.7rem',
    },
  };


const Elemento: React.FC<Props> = ({elemento, anadirCarritoFn}) =>{
    const classesCard = useStylesCard();
    const classesModal= useStylesModal();
    const classesCardModal= useStylesCardModal();
    const [vistaPrevia, setvistaPrevia] = useState(false);
    const mostrarVistaPrevia = () => {
        setvistaPrevia(true);
      };
    
      const ocultarVistaPrevia = () => {
        setvistaPrevia(false);
      }; 


    return (
        <div>

          <Card className={classesCard.root}>
        <CardActionArea>
          <CardMedia
            className={classesCard.media} image={elemento.miniatura} title={elemento.nombre}
          />
          <CardContent >
          <ThemeProvider theme={themeCard}>
            <Typography gutterBottom variant="h3" component="h3" className={classesCard.content}>
            {elemento.nombre}
            </Typography>

            <Typography gutterBottom variant="h3" component="h2">
            Precio: S/. {elemento.precio}
            </Typography>
            </ThemeProvider>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small"variant="outlined"startIcon={<VisibilityIcon />}  onClick={mostrarVistaPrevia}>
            Vista Previa
          </Button>
          <Button size="small" variant="outlined" color="primary" startIcon={<AddShoppingCartIcon />} onClick={()=>anadirCarritoFn(elemento) }>
             Agregar
          </Button>
          <Modal className={classesModal.modal}
            open={vistaPrevia}
            onClose={ocultarVistaPrevia}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            >
                <Card className={classesCardModal.root}>
                    <CardActionArea>
                    <CardMedia
                        className={classesCard.media} image={elemento.miniatura} title={elemento.nombre}
                    />
                    <CardContent>
                    <ThemeProvider theme={themeModal}>
                        <Typography variant="h3">
                        {elemento.nombre}
                        </Typography>
                        <Typography variant="h6">
                          Detalles Adicionales
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                        {elemento.descripcion}
                        </Typography>
                        <Typography gutterBottom variant="h6" component="h6">
                        Precio: S/. {elemento.precio}
                        </Typography>

                    </ThemeProvider>
                    </CardContent>
                    </CardActionArea>
                    <CardActions>
                    <Button variant="outlined" color="primary" startIcon={<AddShoppingCartIcon />} onClick={()=>anadirCarritoFn(elemento)  }>
                    Agregar
                    </Button>
                    </CardActions>
                </Card>
            </Modal>
        </CardActions>
      </Card>   
        </div>


  
    )
}

export default Elemento;